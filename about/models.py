# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class About(models.Model):
    class Meta:
        db_table = "about_section"

    about_text = models.TextField(blank = True)

class UsefulLinks(models.Model):
    class Meta:
        db_table = "useful_links"

    link = models.CharField(max_length=300)
    title = models.CharField(max_length = 60)

    def __unicode__(self):
        return self.title

class Contacts(models.Model):
    class Meta:
        db_table = "site_contacts"

    contacts_text = models.TextField(blank = True)
