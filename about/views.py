# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from about.models import Contacts, About, UsefulLinks

# Create your views here.

def about(request):
    args = {}
    try:
        args['about'] = About.objects.get(id = 1)
    except:
        args['about'] = 'Nothing is available yet'
    return render(request, 'about.html', args)

def contacts(request):
    args = {}
    try:
        args['contacts'] = Contacts.objects.get(id = 1)
    except:
        args['contacts'] =  ['No links are available yet']
    return render(request, 'contacts.html', args)

def ulinks(request):
    args = {}
    try:
        args['ulinks'] = UsefulLinks.objects.all()
    except:
        args['ulinks'] = ['No links are available yet']
    return render(request, 'ulinks.html', args)
