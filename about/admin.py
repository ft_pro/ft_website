# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from about.models import About, UsefulLinks, Contacts

# Register your models here.

class ContactsInLine(admin.StackedInline):
    model = Contacts

class UsefulLinksInLine(admin.StackedInline):
    model = UsefulLinks

class AboutAdmin(admin.ModelAdmin):
    fields = ['about_text']

class ContactAdmin(admin.ModelAdmin):
    fields = ['contacts_text']


class UsefulLinkAdmin(admin.ModelAdmin):
    fields = ['title', 'link']


admin.site.register(About, AboutAdmin)
admin.site.register(Contacts, ContactAdmin)
admin.site.register(UsefulLinks, UsefulLinkAdmin)
