from django.conf.urls import url, include
from about.views import about, contacts, ulinks

urlpatterns = [
		url(r'^about', about, name = 'about'),
                url(r'^contacts', contacts, name = 'contacts'),
                url(r'^ulinks', ulinks, name = 'ulinks'),
	      ]

