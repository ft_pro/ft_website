# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-15 00:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('about_text', models.TextField(blank=True)),
            ],
            options={
                'db_table': 'about_section',
            },
        ),
        migrations.CreateModel(
            name='Contacts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contacts_text', models.TextField(blank=True)),
            ],
            options={
                'db_table': 'site_contacts',
            },
        ),
        migrations.CreateModel(
            name='UsefulLinks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.CharField(max_length=300)),
                ('title', models.CharField(max_length=60)),
            ],
            options={
                'db_table': 'useful_links',
            },
        ),
    ]
