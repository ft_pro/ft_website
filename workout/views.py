from django.shortcuts import render
from django.http.response import HttpResponse, Http404, HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators import csrf
# auth
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.core.paginator import Paginator
from workout.models import Gym, GymLocation, GymContacts, GymStaff, GymMembers
from workout.models import  Workouts, Workout
from workout.models import CoachMember
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import transaction
#forms
from workout.forms import UserSelectForm
# Create your views here.

@csrf_protect
def list_gym_details(request, gym_id = 1):
        args = {}
	args['gym'] = Gym.objects.get(id=gym_id)
	args['gymlocation'] = GymLocation.objects.get(gymlocation_gym_id=gym_id)
	args['gymlocation_city'] = GymLocation.objects.get(gymlocation_gym_id=gym_id)
	args['gymstaff'] = GymStaff.objects.get(gymstaff_gym_id=gym_id)
	args['gymcontacts'] = GymContacts.objects.get(gymcontacts_gym_id=gym_id)
	args['user'] = request.user.id
	return render(request, 'gym_details.html', args)

@login_required(login_url="/auth/login")
@transaction.atomic
def display_workout(request):
        args = {}
        args['sel_user'] = request.user.username
        try:
            coach_id = GymStaff.objects.get(gymstaff_staff = request.user.id).id
            args['flag'] = False
        except GymStaff.DoesNotExist:
            args['flag'] = True
        
        try:
	    args['workouts'] = Workouts.objects.get(workouts_user_id=request.user.id)
        except:
            ws = Workouts(workouts_user_id=request.user.id, workouts_points_total=0)
            ws.save()
            args['user'] = request.user.name + " has not done any workouts"
        else:
            if request.method == 'POST':
                try:
                    su = request.POST['selected_user']
                    args['sel_user'] = User.objects.get(id=su).username 
                    wid = Workouts.objects.get(workouts_user_id=su)
                    args['workout'] = Workout.objects.filter(workout_wid_id=wid.id)
                    return render(request, 'workout_details.html', args) 
                except:
                    render('/')

	    wid = Workouts.objects.get(workouts_user_id=request.user.id)
	    args['workout'] = Workout.objects.filter(workout_wid_id=wid.id)
        finally:
	    return render(request, 'workout_details.html', args)

@login_required
@transaction.atomic
def user_workout(request): # lists users workout
	args = {}
	args['user'] = request.user.username
        if request.method == 'POST':
            try:
                args['sel_user'] = request.POST['selected_user']
            except:
                render('/')
        
        try:
	    args['workouts_user'] = Workouts.objects.get(id = request.user.id)
	    args['workout_user_list'] = Workout.objects.get(workout_wid_id = args['workouts_user'].id)
        except:
            ws = Workouts(workouts_user_id=request.user.id, workouts_points_total=0)
            ws.save()
            args['user'] = request.user.username + " has not done any workout"

	return render(request, 'workouts_details.html', args)

@login_required 
@transaction.atomic
def attached_users1(request): # displays users attached to the coach
        args = {}
        args['coach'] = request.user.username
        try:
    	    coach_id = GymStaff.objects.get(gymstaff_staff = request.user.id).id
        except GymStaff.DoesNotExist:
            return redirect('/')

        #args['form'] = UserSelectForm(coach_id)
        args['user'] = request.user.username
        if request.method == 'POST': 
	    user = []
	    args['form'] = UserSelectForm(coach_id)
            if args['form'].is_valid():
        	    args['choice'] = request.POST['user_name']
	            return HttpResponseRedirect(request,'attached_users.html', args)
            else:
                    return render(request,'attached_users.html',args)
            
        return render(request, 'attached_users.html',args)

@login_required
def attached_users(request):
    args = {}
    args['coach'] = request.user.username
    try:
        coach_id = GymStaff.objects.get(gymstaff_staff = request.user.id).id
    except GymStaff.DoesNotExist:
        return redirect('/')

    args['users'] = User.objects.filter(gymmembers__coachmember__coachmember_coach=coach_id)
     
    
    return render(request,'attached_users.html', args)


