from django.contrib import admin
from workout.models import Gym, GymLocation, GymContacts, GymStaff, GymMembers, CoachMember
# Register your models here.

class GymContactsInLine(admin.StackedInline):
	model = GymContacts
	extra = 1

class GymLocationInLine(admin.StackedInline):
	model = GymLocation
	extra = 1

# User and coach to Gym relationships
class GymStaffInLine(admin.StackedInline):
        model = GymStaff
        extra = 1

class GymMembersInLine(admin.StackedInline):
        model = GymMembers
        extra = 1

class GymCoachMemberInLine(admin.StackedInline):
        model = CoachMember
        extra = 1

class GymAdmin(admin.ModelAdmin):
	fields = ['gym_name']
	inlines = [GymContactsInLine, GymLocationInLine, GymMembersInLine]
	list_filter = ['gym_name']


admin.site.register(Gym, GymAdmin)

class CMembersInLine(admin.StackedInline):
        model = CoachMember 

class GymStaffAdmin(admin.ModelAdmin):
        fields = ['gymstaff_staff']
        inlines = [CMembersInLine]
 #       list_filter = ['coachmember_coach']

admin.site.register(GymStaff, GymStaffAdmin)
