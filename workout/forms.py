from django.db import models
from django import forms
from django.forms import ModelForm
from workout.models import Gym, GymLocation, GymContacts, GymStaff, GymMembers
from workout.models import  Workouts, Workout
from django.contrib.auth.models import User
from workout.models import CoachMember

# USER_CHOISES = GymMembers.objects.filter(coachmember__coachmember_coach=coach_id)

class UserSelectForm(forms.Form):
	user_name = forms.ModelChoiceField(queryset=CoachMember.objects.none())

	def __init__(self, coach_id, *args, **kwargs):
		super(UserSelectForm, self).__init__(*args, **kwargs)
	#	self.fields['user_name'].queryset = GymMembers.objects.filter(coachmember__coachmember_coach=coach_id)
        #   	self.fields['user_name'].queryset = User.objects.filter(gymmembers__coachmember__coachmember_coach=coach_id)
                self.fields['user_name'] = forms.ModelChoiceField(queryset=User.objects.filter(gymmembers__coachmember__coachmember_coach=coach_id))
