# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-08 03:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('workout', '0002_coachmember'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coachmember',
            name='coachmember_coach',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workout.GymStaff'),
        ),
        migrations.AlterField(
            model_name='coachmember',
            name='coachmember_member',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workout.GymMembers'),
        ),
    ]
