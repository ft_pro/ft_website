from django.conf.urls import url, include
from workout.views import list_gym_details, display_workout, attached_users

urlpatterns = [
               	url(r'^gym_details/$', list_gym_details, name = 'list_gym_details'),
		url(r'^workout_details/$', display_workout, name = 'display_workout'),
		url(r'^attached_users/$', attached_users, name = 'attached_users'),
 ]

