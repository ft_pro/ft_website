from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from gyms.models import GymsMap
from django_google_maps import fields as map_fields

# Create your models here.
class Workouts(models.Model):
	class Meta:
		db_table = "workouts"

	workouts_user = models.ForeignKey(User)
	workouts_points_total = models.IntegerField(blank=True)

class Workout(models.Model):
	class Meta:
		db_table = "workout"

	workout_date = models.DateField()
	workout_type = models.TextField(blank=True)
	workout_duration = models.IntegerField(blank=True)
	workout_steps = models.IntegerField(blank=True)
	workout_calories = models.IntegerField(blank=True)
	workout_points = models.IntegerField(blank=True)
	workout_wid = models.ForeignKey(Workouts)

class Gym(models.Model):
	class Meta:
		db_table = "gym"

	gym_name = models.CharField(max_length=30)
	def __unicode__(self):
		return self.gym_name
	#gym_id = models.ForeignKey(GymsMap, null=True, blank=True, default=None)

class GymLocation(models.Model):
	class Meta:
		db_table = "gymlocation"

	gymlocation_gym = models.ForeignKey(Gym, on_delete=models.CASCADE)
	gymlocation_address = models.CharField(max_length=50)
	gymlocation_suburb = models.CharField(max_length=20)
	gymlocation_city = models.CharField(max_length=20)
	gymlocation_country = models.CharField(max_length=20)
	gymlocation_coordinates = models.CharField(max_length=100, blank=True, null=True)

class GymMembers(models.Model):
	class Meta:
		db_table = "gymmembers"

	gymmembers_gym = models.ForeignKey(Gym, on_delete=models.CASCADE)
	gymmembers_members = models.ForeignKey(User, on_delete=models.CASCADE)


class GymStaff(models.Model):
       	class Meta:
                db_table = "gymstaff"
        
        #def __unicode__(self):
        #    return self.id

        gymstaff_gym = models.ForeignKey(Gym, on_delete=models.CASCADE)
        gymstaff_staff = models.ForeignKey(User, on_delete=models.CASCADE)

class GymContacts(models.Model):
	class Meta:
		db_table = "gymcontacts"

	gymcontacts_gym = models.ForeignKey(Gym, on_delete=models.CASCADE)
	gymcontacts_phone = models.CharField(max_length=20, blank=True, null=True)
	gymcontacts_email = models.EmailField(blank=True, null=True)

class CoachMember(models.Model): # defines coach member relationships
	class Meta:
		db_table = "coachmember"
        #def __unicode__(self):
        #        return self.id

	coachmember_member = models.ForeignKey(GymMembers, on_delete=models.CASCADE)
	coachmember_coach = models.ForeignKey(GymStaff, on_delete=models.CASCADE)
