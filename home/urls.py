
from django.conf.urls import url, include
from home.views import home, home_return

urlpatterns = [
			url(r'^home$', home_return, name = 'home_return'),
			url(r'^$', home, name = 'home'),
	      ]
