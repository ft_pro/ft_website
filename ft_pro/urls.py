from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from article.views import *
from workout.views import *
from about.views import *

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^basicview/', include('article.urls')),
	url(r'^auth/', include('loginsys.urls')),
	url(r'^', include('article.urls')),
	url(r'^', include('workout.urls')),
	url(r'^', include('loader.urls')),
	url(r'^', include('home.urls')),
	url(r'^', include('gyms.urls')),
        url(r'^', include('about.urls')),
	] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
