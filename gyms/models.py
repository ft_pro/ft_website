from __future__ import unicode_literals

from django.db import models
from django_google_maps import fields as map_fields

# Create your models here.

class GymsMap(models.Model):
	address = map_fields.AddressField(max_length=200)
	geolocation = map_fields.GeoLocationField(max_length=100)
