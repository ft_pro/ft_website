from django.conf.urls import url, include
from gyms.views import display_map, gym_details

urlpatterns = [
               	url(r'^gyms/$', display_map, name = 'display_map'),
		url(r'^gyms/get/(?P<gym_id>\d+)/$', gym_details, name='gym_details'),
 ]

