
from django.shortcuts import render
from django.shortcuts import render
from django.http.response import HttpResponse, Http404
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response, redirect
from django.core.exceptions import ObjectDoesNotExist
# auth
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
#json
from django.core import serializers
#models
from gyms.models import GymsMap
from workout.models import Gym, GymLocation, GymContacts, GymStaff
# Create your views here.

def display_map(request):
	args = {}
	json_serializer = serializers.get_serializer("json")()
#	gym_coordinates = json_serializer.serialize(Gym.objects.all())
	gym_names = Gym.objects.all()
	gym_name = []
	gym_coord = []
	gym_c = []
	for gn in gym_names:
		gym_name.append(gn.gym_name)
		gym_coord.append(GymLocation.objects.get(gymlocation_gym_id = gn.id))	# gym_coord.append(GymLocation.objects.get(id = gn.id))

	str = ""
	for gc in gym_coord:
			gym_c.append(gc.gymlocation_coordinates)
			str = str + gc.gymlocation_coordinates + "/"

	strn = ""
	for gnam in gym_name:
		strn = strn + gnam + "/"
	gym_address = json_serializer.serialize(GymLocation.objects.all())
	args['gyms'] = strn
	args['coord'] = str
	return render(request, 'gyms.html', args)

def gym_details(request, gym_id):
	args = {}
	args['gym_name'] = Gym.objects.get(id = gym_id).gym_name
	args['gym_contacts'] = GymContacts.objects.get(id = gym_id)
	args['gym_location'] = GymLocation.objects.get(id = gym_id)
        try:
                args['gym_staff'] = User.objects.filter(gymstaff__gymstaff_gym_id = gym_id)
                if not args['gym_staff']:
                    args['gym_staff'] = ["No staff members are available yet"]
        except:
                args['gym_staff'] = ["No staff members are available yet"]
                

	return render(request, 'gym_details.html', args)
