from django.conf.urls import url, include
from loader.views import loader, loaded

urlpatterns = [
               	url(r'^loader/', loader, name = 'loader'),
		url(r'^loaded/', loaded, name = 'loaded'),
	      ]
