from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Article(models.Model):
	class Meta:
		db_table = "article"

	article_title = models.CharField(max_length = 200)
	article_text = models.TextField()
	article_date = models.DateTimeField()
	article_likes = models.IntegerField(default = 0)
	article_image = models.FileField(upload_to='uploads/%Y/%m/%d', blank=True)
	def __unicode__(self):
		return self.article_title

class Comments(models.Model):
       	class Meta:
               	db_table = "comments"

	comments_date = models.DateTimeField(blank=True)
       	comments_text = models.TextField(verbose_name="Add your comment")
       	comments_article = models.ForeignKey(Article)
	comments_from = models.ForeignKey(User)
