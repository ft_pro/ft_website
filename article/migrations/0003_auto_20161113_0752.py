# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-13 07:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0002_auto_20161112_2348'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comments',
            old_name='commebts_article',
            new_name='comments_article',
        ),
        migrations.AlterField(
            model_name='article',
            name='article_likes',
            field=models.IntegerField(default=0),
        ),
    ]
