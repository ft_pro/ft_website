from django.conf.urls import url, include
from loginsys.views import login, logout, register, update_profile

urlpatterns = [
                url(r'^login/', login, name = "login"),
		url(r'^logout/', logout, name = "logout"),
		url(r'^register/', register, name = "register"),
		url(r'^profile/', update_profile, name = "profile")
	      ]

