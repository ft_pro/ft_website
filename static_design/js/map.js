var mymap = L.map('ft_map').setView([174.86, -36.84], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={acpk.eyJ1IjoiZG1zdmFyIiwiYSI6ImNqMzhlNTUwYTAyZDEyd28yendnbXNpMGkifQ.nEwTpMTaPk9gvznekA5pYA}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    accessToken: 'pk.eyJ1IjoiZG1zdmFyIiwiYSI6ImNqMzhlNTUwYTAyZDEyd28yendnbXNpMGkifQ.nEwTpMTaPk9gvznekA5pYA'
}).addTo(mymap);

var marker = L.marker([174.86, -36.84]).addTo(mymap);
